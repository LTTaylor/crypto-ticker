export function toCurrency(value, currency = 'GBP') {
    let amount = parseFloat(value);

    if (Number.isNaN(amount)) {
        amount = 0;
    }

    return amount.toLocaleString('en-GB', {
        style: 'currency',
        currency,
    });
}

export function dateDiffInMinutes(a, b) {
    return Math.floor((b - a) / (1000 * 60));
}
