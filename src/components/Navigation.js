import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Icon from './Coin/Icon';

const Navigation = props => (
    <NavLink
        className="nav__item-link"
        activeClassName="nav__item-link--active"
        to={`/coin/${props.name.toLowerCase()}`}
        title={`View ${props.name} details`}
    >
        <Icon short={props.short} />
    </NavLink>
);

Navigation.propTypes = {
    name: PropTypes.string.isRequired,
    short: PropTypes.string.isRequired,
};

Navigation.defaultProps = {
    name: 'Generic',
    short: 'generic',
};

export default Navigation;
