import React from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';

const Footer = ({ lastUpdated }) => (
    <footer className="coin__footer">
        Last updated: <TimeAgo date={lastUpdated} />
    </footer>
);

Footer.propTypes = {
    lastUpdated: PropTypes.instanceOf(Date).isRequired,
};

const now = new Date();

Footer.defaultProps = {
    lastUpdated: new Date(now.getTime() - 1000),
};

export default Footer;
