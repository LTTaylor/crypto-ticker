import React from 'react';
import PropTypes from 'prop-types';
import { toCurrency } from '../../helpers';

const Price = ({ price, currency }) => <strong>{toCurrency(price, currency)}</strong>;

Price.propTypes = {
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
};

Price.defaultProps = {
    price: 0,
    currency: 'GBP',
};

export default Price;
