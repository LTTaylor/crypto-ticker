import React from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';

const Header = props => (
    <header className="coin__header">
        <h1 className="coin__header__heading">
            {props.name} <span className="coin__header__symbol">{props.symbol}</span>
        </h1>

        <div className="coin__header__icon">
            <Icon short={props.symbol} />
        </div>
    </header>
);

Header.propTypes = {
    name: PropTypes.string.isRequired,
    symbol: PropTypes.string.isRequired,
};

Header.defaultProps = {
    name: 'Stellar',
    symbol: 'XLM',
};

export default Header;
