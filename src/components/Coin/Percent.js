import React from 'react';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';

import arrowPositive from '../../assets/images/arrow-positive.svg';
import arrowNegative from '../../assets/images/arrow-negative.svg';

const Percent = ({ percent }) => (
    <span className={percent < 0 ? `percent percent--negative` : `percent percent--positive`}>
        <span className="percent__text">
            {percent > 0 ? '+' : ''}
            {percent}%
        </span>
        <span
            className={
                percent < 0 ? `percent__arrow percent__arrow--negative` : `percent__arrow percent__arrow--positive`
            }
        >
            <SVG className="percent__arrow-icon" src={percent < 0 ? arrowNegative : arrowPositive} />
        </span>
    </span>
);

Percent.propTypes = {
    percent: PropTypes.number.isRequired,
};

Percent.defaultProps = {
    percent: 0,
};

export default Percent;
