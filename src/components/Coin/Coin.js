import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import MDSpinner from 'react-md-spinner';

import Header from './Header';
import Footer from './Footer';
import Price from './Price';
import Percent from './Percent';

class Coin extends Component {
    state = {
        cryptoData: [],
        loaded: false,
        currency: 'GBP',
        coin: 'stellar',
    };

    componentDidMount() {
        let { coin, currency } = this.props.match.params;
        coin = coin || this.state.coin;
        currency = currency || this.state.currency;

        this.fetchData(coin, currency);
    }

    componentWillReceiveProps(newProps) {
        this.fetchData(newProps.match.params.coin);
    }

    getCachedCryptoData = () => JSON.parse(localStorage.getItem('cryptoData'));

    cacheCryptoData = cryptoData => {
        localStorage.setItem('cryptoData', JSON.stringify(cryptoData));
    };

    fetchData = (coin = 'stellar', currency = 'GBP') => {
        this.setState({
            loaded: false,
        });

        axios
            .get(`https://api.coinmarketcap.com/v1/ticker/${coin}/`, {
                params: {
                    convert: currency,
                },
            })
            .then(response => {
                let cryptoData = [...this.state.cryptoData];

                const objIndex = cryptoData.findIndex(obj => obj.name === coin);

                if (objIndex === -1) {
                    cryptoData.push({ name: coin, data: response.data[0] });
                } else {
                    const updatedCoin = {
                        ...cryptoData[objIndex],
                        data: response.data[0],
                    };

                    cryptoData = [...cryptoData.slice(0, objIndex), updatedCoin, ...cryptoData.slice(objIndex + 1)];
                }

                // this.cacheCryptoData(cryptoData);

                this.setState({
                    cryptoData,
                    loaded: true,
                    coin,
                    currency,
                });
            })
            .catch(error => {
                console.error(error);
            });
    };

    render() {
        const currentCoin = this.state.cryptoData.find(coin => coin.name === this.state.coin);

        if (this.state.loaded === true || typeof currentCoin !== 'undefined') {
            const lastUpdated = new Date(currentCoin.data.last_updated * 1000);

            return (
                <div className="coin">
                    <section className="coin__details">
                        <Header name={currentCoin.data.name} symbol={currentCoin.data.symbol} icon="🚀" />

                        <div>
                            <Price
                                price={parseFloat(currentCoin.data[`price_${this.state.currency.toLowerCase()}`])}
                                currency={this.state.currency}
                            />
                        </div>

                        <div>
                            1h: <Percent percent={parseFloat(currentCoin.data.percent_change_1h)} />
                        </div>

                        <div>
                            24h: <Percent percent={parseFloat(currentCoin.data.percent_change_24h)} />
                        </div>

                        <div>
                            7d: <Percent percent={parseFloat(currentCoin.data.percent_change_7d)} />
                        </div>

                        <Footer lastUpdated={lastUpdated} />
                    </section>
                </div>
            );
        }

        return (
            <div className="coin coin--loading">
                <MDSpinner size="100" singleColor="white" />
            </div>
        );
    }
}

Coin.propTypes = {
    match: PropTypes.shape({
        params: PropTypes.shape({
            coin: PropTypes.string,
            currency: PropTypes.string,
        }),
    }),
};

Coin.defaultProps = {
    match: {
        params: {
            coin: 'stellar',
            currency: 'GBP',
        },
    },
};

export default Coin;
