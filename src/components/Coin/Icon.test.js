import React from 'react';
import { shallow, mount } from 'enzyme';
import Icon from './Icon';

const props = {
    name: 'Stellar',
    short: 'XLM',
};

describe('Icon', () => {
    it('renders without crashing', () => {
        shallow(<Icon {...props} />);
    });
});
