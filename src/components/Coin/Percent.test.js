import React from 'react';
import { shallow, mount } from 'enzyme';
import Percent from './Percent';

const props = {
    percent: 25.91,
};

describe('Percent', () => {
    it('renders without crashing', () => {
        shallow(<Percent />);
    });

    it('default value 0%', () => {
        const wrapper = shallow(<Percent />);
        const price = wrapper.find('.percent__text').text();
        expect(price).toEqual('0%');
    });

    it('percent shows as +25.91%', () => {
        const wrapper = shallow(<Percent {...props} />);
        const percent = wrapper.find('.percent__text').text();
        expect(percent).toEqual('+25.91%');
    });

    it('percent shows as -25.91%', () => {
        const wrapper = shallow(<Percent percent={props.percent * -1} />);
        const percent = wrapper.find('.percent__text').text();
        expect(percent).toEqual('-25.91%');
    });

    it('percent prop equals 25.91', () => {
        const wrapper = mount(<Percent {...props} />);
        expect(wrapper.props().percent).toEqual(25.91);
    });

    it('percent prop equals -25.91', () => {
        const wrapper = mount(<Percent percent={props.percent * -1} />);
        expect(wrapper.props().percent).toEqual(-25.91);
    });

    it('has a class of percent--negative', () => {
        const wrapper = shallow(<Percent percent={props.percent * -1} />);
        expect(wrapper.find('.percent').hasClass('percent--negative')).toEqual(true);
    });

    it('has a class of percent--positive', () => {
        const wrapper = shallow(<Percent {...props} />);
        expect(wrapper.find('.percent').hasClass('percent--positive')).toEqual(true);
    });
});
