import React from 'react';
import { shallow, mount } from 'enzyme';
import Footer from './Footer';

const time = (dateNow, seconds = 1) => new Date(dateNow.getTime() - seconds * 1000);

const now = new Date();

describe('Footer', () => {
    it('renders without crashing', () => {
        shallow(<Footer />);
    });

    it('default value is Last updated: 1 second ago', () => {
        const wrapper = mount(<Footer />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 1 second ago');
    });

    it('shows as last updated 20 seconds ago', () => {
        const lastUpdated = time(now, 20);
        const wrapper = mount(<Footer lastUpdated={lastUpdated} />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 20 seconds ago');
    });

    it('shows as last updated 20 minutes ago', () => {
        const lastUpdated = time(now, 1200);
        const wrapper = mount(<Footer lastUpdated={lastUpdated} />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 20 minutes ago');
    });

    it('shows as last updated 20 hours ago', () => {
        const lastUpdated = time(now, 72000);
        const wrapper = mount(<Footer lastUpdated={lastUpdated} />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 20 hours ago');
    });

    it('shows as last updated 1 week ago', () => {
        const lastUpdated = time(now, 604800);
        const wrapper = mount(<Footer lastUpdated={lastUpdated} />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 1 week ago');
    });

    it('shows as last updated 1 year ago', () => {
        const lastUpdated = time(now, 31536000);
        const wrapper = mount(<Footer lastUpdated={lastUpdated} />);
        const footer = wrapper.text();
        expect(footer).toEqual('Last updated: 1 year ago');
    });
});
