import React from 'react';
import { shallow, mount } from 'enzyme';
import Price from './Price';

const props = {
    price: 5.38,
};

describe('Price', () => {
    it('renders without crashing', () => {
        shallow(<Price />);
    });

    it('default value £0.00', () => {
        const wrapper = shallow(<Price />);
        const price = wrapper.text();
        expect(price).toEqual('£0.00');
    });

    it('price shows as £5.38', () => {
        const wrapper = shallow(<Price price={props.price} />);
        const price = wrapper.text();
        expect(price).toEqual('£5.38');
    });

    it('price prop equals 5.38', () => {
        const wrapper = mount(<Price price={props.price} />);
        expect(wrapper.props().price).toEqual(5.38);
    });
});
