import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SVG from 'react-inlinesvg';
import MDSpinner from 'react-md-spinner';

class Icon extends Component {
    state = {
        icon: null,
        loaded: false,
    };

    componentDidMount() {
        this.loadIcon(icon => this.setState({ icon, loaded: true }));
    }

    loadIcon = callback => {
        const icon = null;

        import(`../../assets/images/white/${this.props.short.toLowerCase()}.svg`).then(module => callback(module));

        return icon;
    };

    render() {
        if (this.state.loaded === true) {
            return (
                <SVG className="nav__icon" src={this.state.icon}>
                    {this.props.short}
                </SVG>
            );
        }

        return <MDSpinner size="50" singleColor="white" />;
    }
}

Icon.propTypes = {
    short: PropTypes.string.isRequired,
};

Icon.defaultProps = {
    short: 'generic',
};

export default Icon;
