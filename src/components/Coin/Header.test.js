import React from 'react';
import { shallow, mount } from 'enzyme';
import Header from './Header';

const props = {
    name: 'Stellar',
    symbol: 'XLM',
};

describe('Header', () => {
    it('renders without crashing', () => {
        shallow(<Header name={props.name} symbol={props.symbol} icon={props.icon} />);
    });

    it('name prop equals Stellar', () => {
        const wrapper = mount(<Header name={props.name} symbol={props.symbol} icon={props.icon} />);
        expect(wrapper.props().name).toEqual('Stellar');
    });

    it('symbol prop equals XLM', () => {
        const wrapper = mount(<Header name={props.name} symbol={props.symbol} icon={props.icon} />);
        expect(wrapper.props().symbol).toEqual('XLM');
    });

    it('default value is Stellar XLM', () => {
        const wrapper = shallow(<Header />);
        const header = wrapper.find('h1').text();
        expect(header).toEqual('Stellar XLM');
    });
});
