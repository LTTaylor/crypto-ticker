import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import Coin from './components/Coin/Coin';
import Navigation from './components/Navigation';

import './css/App.css';

const coins = [
    // { name: 'Binance-Coin', short: 'BNB' },
    { name: 'Bitcoin', short: 'BTC' },
    { name: 'Enigma-Project', short: 'ENG' },
    { name: 'Ethereum', short: 'ETH' },
    { name: 'Litecoin', short: 'LTC' },
    { name: 'Neblio', short: 'NEBL' },
    { name: 'Poet', short: 'POE' },
    { name: 'Ripple', short: 'XRP' },
    { name: 'Stellar', short: 'XLM' },
    { name: 'VeChain', short: 'VEN' },
    { name: 'Waltonchain', short: 'WTC' },
];

class App extends Component {
    componentWillMount() {
        document.addEventListener('keydown', this.handleKeyPress);
        document.addEventListener('mousedown', this.handleMouseDown);
    }

    handleKeyPress = e => {
        if (e.keyCode === 9) {
            document.getElementById('js-body').classList.add('tab-highlight');
        }
    };

    handleMouseDown = () => {
        document.getElementById('js-body').classList.remove('tab-highlight');
    };

    render() {
        return (
            <Router basename={process.env.PUBLIC_URL}>
                <Route
                    render={({ location }) => (
                        <div className="App">
                            <TransitionGroup component="main">
                                <CSSTransition key={location.key} classNames="coin--fade" timeout={200}>
                                    <Switch location={location}>
                                        <Route exact path="/" component={Coin} />
                                        <Route exact path="/coin/:coin/:currency?" component={Coin} />
                                    </Switch>
                                </CSSTransition>
                            </TransitionGroup>

                            <header className="header">
                                <nav className="nav">
                                    <ul className="nav__items">
                                        {coins.map(coin => (
                                            <li className="nav__item" key={coin.short}>
                                                <Navigation {...coin} />
                                            </li>
                                        ))}
                                    </ul>
                                </nav>
                            </header>
                        </div>
                    )}
                />
            </Router>
        );
    }
}

const styles = {};

styles.fill = {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
};

styles.content = {
    ...styles.fill,
    top: '40px',
    textAlign: 'center',
};

styles.nav = {
    padding: 0,
    margin: 0,
    position: 'absolute',
    top: 0,
    height: '40px',
    width: '100%',
    display: 'flex',
};

styles.navItem = {
    textAlign: 'center',
    flex: 1,
    listStyleType: 'none',
    padding: '10px',
};

styles.hsl = {
    ...styles.fill,
    color: 'white',
    paddingTop: '20px',
    fontSize: '30px',
};

styles.rgb = {
    ...styles.fill,
    color: 'white',
    paddingTop: '20px',
    fontSize: '30px',
};

export default App;
