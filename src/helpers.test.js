import { toCurrency } from './helpers';

describe('toCurrency', () => {
    it('displays as £0.00', () => {
        expect(toCurrency(0)).toEqual('£0.00');
    });

    it('displays as £20.50', () => {
        expect(toCurrency(20.5)).toEqual('£20.50');
    });

    it('displays as -£20.50', () => {
        expect(toCurrency(-20.5)).toEqual('-£20.50');
    });

    it('displays as £10.50', () => {
        expect(toCurrency(20.5 - 10)).toEqual('£10.50');
    });

    it('displays as £0.00', () => {
        expect(toCurrency('abcdefg')).toEqual('£0.00');
    });

    it('displays as £1.00', () => {
        expect(toCurrency('1')).toEqual('£1.00');
    });

    it('displays as £1000.50', () => {
        expect(toCurrency('1000.5', 'GBP')).toEqual('£1,000.50');
    });

    it('displays as €1000.50', () => {
        expect(toCurrency('1000.5', 'EUR')).toEqual('€1,000.50');
    });

    it('displays as ¥1000.50', () => {
        expect(toCurrency('1000.5', 'JPY')).toEqual('¥1,001');
    });
});
